use std::str::FromStr;

fn main() {
    let problem_input : Day8Input = std::fs::read_to_string("input/prod_input.txt")
        .expect("Could not open file!")
        .parse()
        .expect("Could not parse input file!");

    let p1_answer = problem_input.solve_p1(true);
    let p2_answer = problem_input.solve_p1(false);

    println!("Part 1 Answer: {p1_answer}");
    println!("Part 2 Answer: {p2_answer}");
}

struct Day8Input {
    sensor_values : Vec<Vec<i64>>,
}

#[derive(Debug)]
enum ParseError {

}

impl FromStr for Day8Input {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let sensor_values = s.lines()
        .map(
            |line| line.split_ascii_whitespace()
            .map(|num| num.parse::<i64>().unwrap())
            .collect::<Vec<i64>>()
        )
        .collect::<Vec<Vec<i64>>>();
        Ok(Day8Input {
            sensor_values
        })
    }
}

impl Day8Input {
    fn solve_p1(&self, last : bool) -> i64 {
        self.sensor_values.iter()
            .map(|sensor| predict_next_value(sensor, last))
            .sum()
    }
}

fn predict_next_value(values : &[i64], last : bool) -> i64 {
    let mut values = values.to_owned();
    if values.iter().all(|i| *i == 0_i64) {
        return 0;
    }
    let mut lv_stack = Vec::new();
    if last {
        lv_stack.push(*values.last().unwrap());
        while values.len() > 0 && !values.iter().all(|i| *i == 0_i64) {
            values = values.windows(2).map(|window| window[1] - window[0]).collect();
            lv_stack.push(*values.last().unwrap());
        }
        lv_stack.iter().sum()
    } else {
        lv_stack.push(*values.first().unwrap());
        while values.len() > 0 && !values.iter().all(|i| *i == 0_i64) {
            values = values.windows(2).map(|window| window[1] - window[0]).collect();
            lv_stack.push(*values.first().unwrap());
        }
        lv_stack.iter().rev().fold(0, |acc, lv| *lv - acc)
    }
}
