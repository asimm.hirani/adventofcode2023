use std::{num::ParseIntError, collections::BinaryHeap, str::FromStr, cmp::Reverse};

fn main() -> anyhow::Result<()>{

    // Read problem input
    let input = std::fs::read_to_string("input/prod_data.txt")?;
    // Generate problem set
    let day_param : Day5Input = input.parse()?;
    let answer_p1 = day_param.solve_p1();
    println!("Part1 Answer: {answer_p1}");
    let answer_p2 = day_param.solve_p2();
    println!("Part2 Answer: {answer_p2}");
    Ok(())
}

#[derive(Eq, Debug)]
struct Range {
    destination_start : u64,
    source_start : u64,
    length : u64
}

impl PartialOrd for Range {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.source_start.partial_cmp(&other.source_start)
    }
}

impl Ord for Range {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl PartialEq for Range {
    fn eq(&self, other: &Self) -> bool {
        self.source_start == other.source_start
    }
}


#[derive(Debug)]
enum RangeParseError {
    ParseError(ParseIntError),
    InvalidNumberOfElements,
}

impl From<ParseIntError> for RangeParseError {
    fn from(value: ParseIntError) -> Self {
        Self::ParseError(value)
    }
}

impl Range {
    fn from_line(line : &str) -> Result<Self, RangeParseError> {
        let line_vec : Vec<&str> = line.split(' ').collect();
        if line_vec.len() != 3 {return Err(RangeParseError::InvalidNumberOfElements)};
        let output = Range {
            destination_start: line_vec[0].parse()?,
            source_start: line_vec[1].parse()?,
            length: line_vec[2].parse()?,
        };
        Ok(output)

    }

    fn map(&self, test_point : u64) -> Option<u64> {
        if test_point < self.source_start || test_point >= self.source_start + self.length {
            None
        } else {
            let distance = test_point - self.source_start;
            Some(self.destination_start + distance)
        }
    }
}

#[derive(Debug)]
struct RangeMap {
    mapped_ranges : Vec<Range>,
    start_values : Vec<u64>,
}

impl RangeMap {
    fn from_block(block : &str) -> Self {
        let mut mapped_ranges = Vec::new();
        for line in block.lines() {
            mapped_ranges.push(Range::from_line(line).expect("Could not parse line!"));
        }
        mapped_ranges.sort_unstable();
        let mut start_values = Vec::new();
        for range in &mapped_ranges {
            start_values.push(range.source_start)
        }
        RangeMap { mapped_ranges, start_values }
    }


    fn probe(&self, input : u64) -> u64 {
        if input < self.start_values[0] {
            return input;
        }
        // Binary search for the range closest range whose start is less than or equal to input
        let partition_point = match self.start_values.binary_search(&input) {
            Ok(p) => p,
            Err(p) => if p != 0 {p - 1} else {p}
        };
        // The point is either in this range, or it is not
        self.mapped_ranges[partition_point].map(input).unwrap_or(input)
    }
}


#[derive(Debug)]
struct Day5Input {
    seed_list : Vec<u64>,
    p2_seed_list : Vec<std::ops::Range<u64>>,
    range_maps : Vec<RangeMap>,
}


impl FromStr for Day5Input {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // Line Iterator from input
        // First line is the seeds
        let mut line_iter = s.lines();
        let seed_line = line_iter.next().ok_or(anyhow::Error::msg("Not enough lines in file!"))?;
        // Split the seed line
        let seed_list : Vec<u64> = seed_line
            .split(':')
            .nth(1)
            .ok_or(anyhow::Error::msg("Invalid seed line!"))?
            .trim()
            .split(' ')
            .map(|cnum| cnum.parse::<u64>())
            .collect::<Result<Vec<u64>, ParseIntError>>()?;
        let mut range_map_pad = String::new();
        let mut range_maps = Vec::new();
        for line in line_iter {
            if line.starts_with(|c: char| c.is_ascii_digit()) {
                range_map_pad.push_str(line);
                range_map_pad.push('\n')
            } else {
                let map = RangeMap::from_block(&range_map_pad);
                if map.mapped_ranges.is_empty() {
                    continue;
                }
                range_maps.push(map);
                range_map_pad = "".to_owned();
            }
        }
        range_maps.push(RangeMap::from_block(&range_map_pad));

        // Parse seed list as p2
        let mut p2_seed_list = Vec::new();
        for chunk in seed_list.chunks(2) {
            p2_seed_list.push(chunk[0]..chunk[0]+chunk[1])
        }
        Ok(Day5Input {seed_list, p2_seed_list, range_maps})
    }
}

impl Day5Input {
    fn solve_p1(&self) -> u64 {
        let mut land_heap = BinaryHeap::new();
        for seed in &self.seed_list {
            let mut input = *seed;
            for map in &self.range_maps {
                let output = map.probe(input);
                input = output;
            }
            land_heap.push(Reverse(input));

        }
        let answer = land_heap.peek().expect("No seeds!").0;
        answer
    }

    fn solve_p2(&self) -> u64 {
        let mut minimum_location = u64::MAX;
        for seed_range in &self.p2_seed_list {
            for seed in seed_range.clone() {
                let mut input = seed;
                for map in &self.range_maps {
                    input = map.probe(input);
                }
                minimum_location = input.min(minimum_location);
            }
        }
        minimum_location
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_range_probe_below() {
        let range = Range { destination_start: 33, source_start: 10, length: 22 };
        let output = range.map(0);
        assert_eq!(output, None)
    }

    #[test]
    fn test_range_boundary() {
        let range = Range { destination_start: 33, source_start: 10, length: 22 };
        let output = range.map(31);
        assert_eq!(output, Some(54))
    }

    #[test]
    fn test_range_boundary2() {
        let range = Range { destination_start: 33, source_start: 10, length: 22 };
        let output = range.map(32);
        assert_eq!(output, None)
    }

    #[test]
    fn test_range_boundary3() {
        let range = Range { destination_start: 33, source_start: 10, length: 22 };
        let output = range.map(10);
        assert_eq!(output, Some(33))
    }
}