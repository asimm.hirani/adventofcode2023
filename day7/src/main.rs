use anyhow::anyhow;
fn main() {
    let input = std::fs::read_to_string("input/prod_input.txt").unwrap();
    let input = Day7Input::try_from(input.as_str()).unwrap();
    let p1_answer = input.solve_p1(false);
    let p2_answer = input.solve_p1(true);
    println!("Part 1 Answer: {p1_answer}");
    println!("Part 2 Answer: {p2_answer}");
}


struct Day7Input {
    hand_vec : Vec<(String, u64)>
}

impl TryFrom<&str> for Day7Input {
    type Error = anyhow::Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let mut hand_vec = Vec::new();
        for line in value.lines() {
            let hand = line.split_ascii_whitespace().nth(0).ok_or(anyhow!("Incorrect format!"))?;
            let bid = line.split_ascii_whitespace().nth(1).ok_or(anyhow!("Incorrect format!"))?;
            hand_vec.push((hand.to_owned(), bid.parse::<u64>()?))
        }
        Ok(Self {hand_vec})
    }
}

impl Day7Input {
    fn solve_p1(&self, joker : bool) -> u64{
        let mut hand_vec = Vec::new();
        for (string, bid) in &self.hand_vec {
            let hand = Hand::from_str(&string, joker);
            hand_vec.push((hand, bid));
        }
        hand_vec.sort_unstable_by(|a, b| a.0.cmp(&b.0));

        let mut accumulator = 0;
        for (rank, (hand , bid)) in hand_vec.iter().enumerate() {
            let rank = rank + 1;
            println!("Hand {hand:?} with bid {bid} at rank {rank}");
            accumulator += rank as u64 * *bid;
        }
        accumulator
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Debug)]
enum Card {
    Joker,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace,
}


impl From<Card> for usize {
    fn from(value: Card) -> Self {
        match value {
            Card::Joker => 14,
            Card::Two => 2,
            Card::Three => 3,
            Card::Four => 4,
            Card::Five => 5,
            Card::Six => 6,
            Card::Seven => 7,
            Card::Eight => 8,
            Card::Nine => 9,
            Card::Ten => 10,
            Card::Jack => 11,
            Card::Queen => 12,
            Card::King => 13,
            Card::Ace => 1,
        }
    }
}

#[derive(Debug)]
enum CardFromCharError {
    InvalidCharacter,
}
impl Card {

    fn try_from(value: char, joker : bool) -> Result<Self, CardFromCharError> {
        match value {
            'A' => Ok(Self::Ace),
            'K' => Ok(Self::King),
            'Q' => Ok(Self::Queen),
            'J' => {if joker {Ok(Self::Joker)} else {Ok(Self::Jack)}},
            'T' => Ok(Self::Ten),
            '9' => Ok(Self::Nine),
            '8' => Ok(Self::Eight),
            '7' => Ok(Self::Seven),
            '6' => Ok(Self::Six),
            '5' => Ok(Self::Five),
            '4' => Ok(Self::Four),
            '3' => Ok(Self::Three),
            '2' => Ok(Self::Two),
            _ => Err(CardFromCharError::InvalidCharacter)
        }
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug)]
enum HandLabel {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

impl HandLabel {
    fn grade_hand(cards : &[Card]) -> Self {
        let mut counts = vec![0; 15];
        for card in cards {
            counts[*card as usize] += 1;
        }
        let joker_count = counts.remove(Card::Joker as usize);
        //println!("There are {joker_count} jokers in {cards:?}");

        counts.sort_unstable();
        let max_counts = counts.pop().unwrap() + joker_count;
        match max_counts {
            1 => Self::HighCard,
            2 => {
                if counts.pop().unwrap() == 2 {
                    Self::TwoPair
                } else {
                    Self::OnePair
                }
            },
            3 => {
                if counts.pop().unwrap() == 2 {
                    Self::FullHouse
                } else {
                    Self::ThreeOfAKind
                }
            },
            4 => Self::FourOfAKind,
            5.. => Self::FiveOfAKind,
            _ => panic!("Unexpected number of matching cards!")
        }
    }
}


#[derive(PartialEq, Eq, PartialOrd, Ord, Debug)]
struct Hand {
    label : HandLabel,
    cards : Vec<Card>
}

impl Hand {
    fn from_str(hand : &str, joker : bool) -> Self {
        let cards : Vec<Card> = hand.chars().map(|c| Card::try_from(c, joker).expect("Invalid character!")).collect();
        let label = HandLabel::grade_hand(&cards);

        // println!("Graded hand {cards:?} as {label:?}");
        Hand {
            label,
            cards
        }

    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basic_hand_parse() {
        let input = "KK677";
        let hand = Hand::from_str(input, false);
        let hand_type = HandLabel::TwoPair;
        let parsed_hand = vec![Card::King, Card::King, Card::Six, Card::Seven, Card::Seven];
        assert_eq!(parsed_hand, hand.cards);
        assert_eq!(hand_type, hand.label);
    }

    #[test]
    fn basic_hand_parse_compare() {
        let input = "KK677";
        let input2 = "KTJJT";

        let hand = Hand::from_str(input, false);
        let hand2 = Hand::from_str(input2, false);

        let hand_type = HandLabel::TwoPair;
        let hand_type2 = HandLabel::TwoPair;

        let parsed_hand = vec![Card::King, Card::King, Card::Six, Card::Seven, Card::Seven];
        let parsed_hand2 = vec![Card::King, Card::Ten, Card::Jack, Card::Jack, Card::Ten];

        assert_eq!(parsed_hand, hand.cards);
        assert_eq!(hand_type, hand.label);
        assert_eq!(parsed_hand2, hand2.cards);
        assert_eq!(hand_type2, hand2.label);
        assert!(hand > hand2);
    }

    #[test]
    fn test_compare2() {
        let input1 = "T55J5";
        let input2 = "QQQJA";

        let hand1 = Hand::from_str(input1, false);
        let hand2 = Hand::from_str(input2, false);

        assert_eq!(hand1.label, hand2.label);
        assert!(hand2 > hand1);
    }

    #[test]
    fn test_compare3() {
        let input1 = "KK677";
        let input2 = "QQQJA";

        let hand1 = Hand::from_str(input1, false);
        let hand2 = Hand::from_str(input2, false);

        assert_eq!(HandLabel::TwoPair, hand1.label);
        assert_eq!(HandLabel::ThreeOfAKind, hand2.label);

        assert!(hand2 > hand1);
    }
}