use std::str::FromStr;

use ahash::HashMap;
fn main() {
    let problem_input : GraphInput = 
        std::fs::read_to_string("test_data/prod_data.txt")
        .expect("Could not find file at path!")
        .parse()
        .expect("Could not parse problem input!");
    
    let p1_soln = problem_input.solve_p1();
    let p2_soln = problem_input.solve_p2();

    println!("Part 1 Solution: {p1_soln}");
    println!("Part 2 Solution: {p2_soln}");
}

struct GraphInput {
    direction_list : Vec<Direction>,
    graph : NodeGraph,
    p2_nodes : Vec<[char; 3]>
}

impl GraphInput {
    fn solve_p1(&self) -> u64 {
        let mut traverser = GraphTraverser::from_graph(&self.graph, &['A','A','A']);
        let mut num_steps = 0;
        while traverser.get_current_node() != ['Z', 'Z', 'Z'] {
            let next_direction = &self.direction_list[num_steps % &self.direction_list.len()];
            traverser.move_direction(next_direction);
            num_steps += 1;
        }

        num_steps as u64
    }

    fn solve_p2(&self) -> u64 {
        let mut traversers : Vec<GraphTraverser> = self.p2_nodes
            .iter()
            .map(|start_node| GraphTraverser::from_graph(&self.graph, start_node))
            .collect();
        let mut num_steps = 0;
        let mut cycles_vec = vec![0; self.p2_nodes.len()];
        while !cycles_vec.iter().all(|cycle_len| cycle_len != &0_usize) {
            let next_direction = &self.direction_list[num_steps % &self.direction_list.len()];
            for (idx, traverser) in traversers.iter_mut().enumerate() {
                traverser.move_direction(next_direction);
                if cycles_vec[idx] == 0 && traverser.get_current_node()[2] == 'Z' {
                    cycles_vec[idx] = num_steps + 1;
                }
            }
            num_steps += 1;

            if num_steps.is_power_of_two() {
                println!("Just passed {num_steps} iterations!");
            }
        }

        cycles_vec.iter().fold(1_u64, |acc, steps| lcm(acc, *steps as u64))
    }
}

#[derive(Debug)]
enum GraphError {
    InvalidDirection,
    NotEnoughLines,
    InvalidLineFormat
}

impl FromStr for GraphInput {
    type Err = GraphError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut line_iter = s.lines();
        // First line is a direction
        let directions = 
            parse_direction_line(
                line_iter
                    .next()
                    .ok_or(GraphError::NotEnoughLines)?
            )?;
        // Consume the rest of the lines to create the graph
        let mut graph = NodeGraph::new();
        let mut p2_nodes: Vec<[char;3]> = Vec::new();
        
        for line in line_iter {
            let (node_name, left, right) = parse_graph_line(line)?;
            if node_name.ends_with('A') {
                p2_nodes.push(node_name.chars().collect::<Vec<char>>().try_into().expect("Could not parse node!"));
            }
            graph.add_or_update_node(node_name, left, right);
        }
        println!("P2 Nodes: {p2_nodes:?}");

        Ok(GraphInput {
            direction_list: directions,
            graph: graph,
            p2_nodes
        })
    }
}

enum Direction {
    Left,
    Right
}

fn parse_direction_line(line : &str) -> Result<Vec<Direction>, GraphError> {
    let mut result = Vec::new();
    for ch in line.chars() {
        let append = match ch {
            'L' => Direction::Left,
            'R' => Direction::Right,
            _ => return Err(GraphError::InvalidDirection)
        };
        result.push(append)
    }
    Ok(result)
}

fn parse_graph_line(line : &str) -> Result<(&str, &str, &str), GraphError> {
    let byte_line = line.as_bytes();
    if byte_line.len() != 16 {
        Err(GraphError::InvalidLineFormat)
    } else {
        Ok((&line[0..3], &line[7..10], &line[12..15]))
    }
}

struct GraphTraverser<'a> {
    current_pos : usize,
    graph : &'a NodeGraph
}

impl<'a> GraphTraverser<'a> {
    fn move_direction(&mut self, direction : &Direction){
        let destination_node = 
            match direction {
                Direction::Left => self.graph.node_vec[self.current_pos].left,
                Direction::Right => self.graph.node_vec[self.current_pos].right,
            }
            .expect("Ran into a dead end!");
        let destination_idx = 
            self.graph.map.get(&destination_node).expect("Node not in graph!");
        self.current_pos = *destination_idx;
    }

    fn from_graph(value: &'a NodeGraph, start : &[char; 3]) -> Self {
        GraphTraverser {
            current_pos : *value.map.get(start).expect("Node AAA not in graph!"),
            graph: value
        }
    }

    fn get_current_node(&self) -> [char; 3] {
        self.graph.node_vec[self.current_pos].name.clone()
    }
}


struct NodeGraph {
    map : HashMap<[char;3], usize>,
    node_vec : Vec<GraphNode>,
}

impl NodeGraph {
    fn new() -> Self {
        NodeGraph {
            map : HashMap::default(),
            node_vec : Vec::new()
        }
    }

    fn create_node(&mut self, node_name : &[char; 3]) -> usize {
        if let Some(idx) = self.map.get(node_name) {
            *idx
        } else {
            let empty_node = GraphNode { name: node_name.to_owned(), left : None, right: None };
            self.node_vec.push(empty_node);
            let idx = self.node_vec.len() - 1;
            self.map.insert(*node_name, idx);
            idx
        }
    }
    
    fn add_or_update_node(&mut self, node_name : &str, left_node : &str, right_node : &str) {
        let node_name : [char; 3] = node_name
            .chars()
            .collect::<Vec<char>>()
            .try_into()
            .expect("Wrong number of chars in node_name!");
        let left_node : [char; 3] = left_node
            .chars()
            .collect::<Vec<char>>()
            .try_into()
            .expect("Nodes must be 3 chars in length!");
        let right_node : [char; 3] = right_node
            .chars()
            .collect::<Vec<char>>()
            .try_into()
            .expect("Node names must be 3 characters in length!");

        let node = GraphNode {
            name: node_name,
            left: Some(left_node),
            right: Some(right_node),
        };

        // Create node_name, left_node, and right_node if they don't already exist
        self.create_node(&left_node);
        self.create_node(&right_node);

        let node_idx = self.create_node(&node_name);
        self.node_vec[node_idx] = node;
    }
}

struct GraphNode {
    name : [char; 3],
    left : Option<[char;3]>,
    right : Option<[char;3]>,
}

fn lcm(a : u64, b : u64) -> u64 {
    a / gcd(a, b) * b
}

fn gcd(a : u64, b : u64) -> u64 {
    let mut a = a;
    let mut b = b;
    while a != b {
        if a > b {
            a = a - b
        } else {
            b = b - a
        }
    }
    a
}