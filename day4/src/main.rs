fn main() {
    let problem_input = std::fs::read_to_string("data/prod_data.txt").unwrap();
    let p1 = solve_day4_p1(&problem_input);
    let p2 = solve_day4_p2(&problem_input);

    println!("p1: {}", p1);
    println!("p2: {}", p2);
}

fn solve_day4_p1(input : &str) -> u64 {
    // Split into lines
    let mut score = 0;
    for line in input.split("\n") {
        score += GameCard::from_line(line).score();
    }
    score
}

fn solve_day4_p2(input : &str) -> u64 {
    let mut gamecard_vec = Vec::new();
    for line in input.split("\n") {
        gamecard_vec.push(GameCard::from_line(line));
    }
    let mut card_winnings_map: Vec<(u64, usize)> = Vec::new();
    for card in &gamecard_vec {
        card_winnings_map.push((card.card_no,card.num_matches()));
    }

    TheGame {
        qty_list : vec![1; gamecard_vec.len()],
        card_winnings_map
    }.solve()
    
}

struct TheGame {
    qty_list : Vec<u64>,
    card_winnings_map : Vec<(u64, usize)>,
}

impl TheGame {
    // Iterate the game, outputting the sum of the total number of cards won.
    fn solve(&mut self) -> u64 {
        for (card_no, winnings) in &self.card_winnings_map {
            let card_qty = self.qty_list[*card_no as usize - 1];
            let card_nos_won = card_no+1..card_no+1+*winnings as u64;

            // println!("Card number {} won {} cards, adding {:?} cards!", card_no, winnings, card_nos_won);
            for card in card_nos_won {
                self.qty_list[card as usize - 1] += card_qty;
            }
        }
        self.qty_list.iter().sum()
    }
}

#[derive(Debug, PartialEq, Eq)]
struct GameCard {
    card_no : u64,
    winning_numbers : Vec<u64>,
    your_numbers : Vec<u64>,
}

impl GameCard {
    fn from_line(line : &str) -> GameCard {
        // Split the line into the game_number and the numbers.
        let mut line_iterator = line.split(":");
        // Get the game number.
        let card_no = extract_number(line_iterator.next().unwrap());
        // Split at '|'
        let mut game_num_iter = line_iterator.next().unwrap().split("|");
        let mut winning_numbers = decode_space_vec(game_num_iter.next().unwrap());
        winning_numbers.sort_unstable_by(|a, b| b.cmp(a));
        let mut your_numbers = decode_space_vec(game_num_iter.next().unwrap());
        your_numbers.sort_unstable_by(|a, b| b.cmp(a));
        // println!("Card number: {}", card_no);
        GameCard { card_no, winning_numbers, your_numbers }
    }
    
    fn num_matches(&self) -> usize {
        let mut matches = 0;
        let mut cursor = 0;
        for winning_number in &self.winning_numbers {
            while cursor < self.your_numbers.len() && &self.your_numbers[cursor] >= winning_number {
                if winning_number == &self.your_numbers[cursor] {
                    matches += 1;
                }
                cursor += 1;
            }
        }
        matches
    }

    fn score(&self) -> u64 {
        let matches = self.num_matches() as u32;
        if matches > 0 {
            2_u64.pow(matches-1)
        } else {
            0
        }
    }
}

fn extract_number(input : &str) -> u64 {
    for substr in input.split_ascii_whitespace() {
        if let Ok(num) = substr.parse::<u64>() {
            return num
        }
    }
    unreachable!("Did not find number in input!");
}

fn decode_space_vec(input : &str) -> Vec<u64> {
    let mut return_val = Vec::new();
    for candidate in input.split_ascii_whitespace() {
        if let Ok(num) = candidate.parse::<u64>() {
            return_val.push(num)
        }
    }
    return_val
} 

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_line() {
        let input = "Card  34: 33 22 55 66 10 | 33 44 72 55 100 253 10 22";
        let output = GameCard::from_line(input);
        let answer = GameCard {
            card_no: 34,
            winning_numbers: vec![66, 55, 33, 22, 10],
            your_numbers: vec![253, 100, 72, 55, 44, 33, 22, 10],
        };
        assert_eq!(output, answer);
    }
}