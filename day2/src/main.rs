use std::path::Path;

fn main() {
    let raw_data = load_data("data/input.txt");
    let p1_soln = solve_day2_p1(&raw_data);
    let p2_soln = solve_day2_p2(&raw_data);
    println!("p1: {}", p1_soln);
    println!("p2: {}", p2_soln);
}

fn load_data<P: AsRef<Path>>(path_to_data : P) -> String {
    std::fs::read_to_string(path_to_data).unwrap()
}

#[derive(Debug, PartialEq, Eq)]
struct GameResult {
    num_red : u64,
    num_blue : u64,
    num_green : u64,
}

impl GameResult {
    fn power(&self) -> u64 {
        self.num_blue * self.num_green * self.num_red
    }
}

fn get_game_results_from_line(game : &str) -> Vec<GameResult> {
    let mut result_vec = Vec::new();

    // Split the game at ;
    let game_iter = game.split(";");
    for game_raw in game_iter {
        result_vec.push(parse_game_result(game_raw));
    }
    result_vec
}

fn parse_game_result(game_raw : &str) -> GameResult {
    let mut return_val = GameResult {
        num_red: 0,
        num_blue: 0,
        num_green: 0,
    };
    // Split the line at "," this should yield up to 3 elements in the iterator
    let ball_group_iter = game_raw.split(",");
    for group in ball_group_iter {
        // Trim the whitespace on the group
        let group = group.trim();
        // Split the group at " ", this will separate the number from the color
        let mut group_iter = group.split(" ");
        // The quantity is the first element in the iterator
        let ball_qty = group_iter.next().unwrap().parse::<u64>().unwrap();
        // The ball color is the next element
        let ball_color = group_iter.next().unwrap();
        match ball_color {
            "red" => return_val.num_red = ball_qty,
            "green" => return_val.num_green = ball_qty,
            "blue" => return_val.num_blue = ball_qty,
            _ => panic!("Unexpected color: {}", ball_color)
        }
    }
    return_val
}

/// Gets the game number and the rest of the line for further processing
fn get_game_number_from_line(line : &str) -> (u64, &str) {
    // Split at : because it only shows up once in each line
    let mut iter = line.split(":");
    // We know that the line starts with "Game " then the number
    let game_number_str = &iter.next().unwrap()[5..];
    
    if let Ok(number) = game_number_str.parse::<u64>() {
        (number, &iter.next().unwrap()[1..])
    } else {
        panic!("Cannot parse game number")
    }
}

/// Finds the minimum number of balls of each color required for a game's results
fn minimum_colors_from_game_results(game_results : &[GameResult]) -> GameResult {
    let (mut min_red, mut min_blue, mut min_green) = (0, 0, 0);
    for game in game_results {
        if min_red < game.num_red {
            min_red = game.num_red;
        }
        if min_blue < game.num_blue {
            min_blue = game.num_blue;
        }
        if min_green < game.num_green {
            min_green = game.num_green;
        }
    }
    GameResult { num_red: min_red, num_blue: min_blue, num_green: min_green }
}

fn is_game_possible_for_given_combination(game_results : &[GameResult], target_combination : &GameResult) -> bool {
    let min_colors = minimum_colors_from_game_results(game_results);
    if target_combination.num_red < min_colors.num_red {return false}
    if target_combination.num_green < min_colors.num_green {return false}
    if target_combination.num_blue < min_colors.num_blue {return false}

    true
}

fn solve_day2_p1(input_data : &str) -> u64{
    // Define the target we are looking for
    let target_combination = GameResult {
        num_red: 12,
        num_blue: 14,
        num_green: 13,
    };
    
    let mut answer_accumulator = 0;
    // Split the data by line
    let line_iter = input_data.split("\n");
    for line in line_iter {
        // Print the line 
        // Get the game id
        let (game_id, game_raw) = get_game_number_from_line(line);
        // Parse the game results
        let game_results = get_game_results_from_line(game_raw);
        // Compare against the p1 target
        if is_game_possible_for_given_combination(&game_results, &target_combination) {
            answer_accumulator += game_id;
        }
    }
    answer_accumulator
}

fn solve_day2_p2(input_data : &str) -> u64 {
    let mut power_accumulator = 0;
    let line_iter = input_data.split("\n");
    for line in line_iter {
        let (_, game_raw) = get_game_number_from_line(line);
        let game_results = get_game_results_from_line(game_raw);
        let min_cubes = minimum_colors_from_game_results(&game_results);
        power_accumulator += min_cubes.power();
    }
    power_accumulator
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_game_number() {
        let answer = 42;
        let input = "Game 42: 2 blue, 3 green, 16 red; 1 blue, 3 red; 2 green, 13 red; 18 red, 2 blue, 1 green; 3 red, 1 blue";

        let result = get_game_number_from_line(input);
        assert_eq!((answer, "2 blue, 3 green, 16 red; 1 blue, 3 red; 2 green, 13 red; 18 red, 2 blue, 1 green; 3 red, 1 blue"), result);
    }

    #[test]
    fn game_result_parser() {
        let answer = GameResult {
            num_red: 3,
            num_blue: 5,
            num_green: 0,
        };

        let input = "5 blue, 3 red";
        let result = parse_game_result(input);
        assert_eq!(answer, result);
    }
}