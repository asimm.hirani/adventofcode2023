use rayon::prelude::*;

fn main() {
    let test_data_path = "prod_data/problem_data.txt";
    println!("{}", solve_day1_p1(&test_data_path));
    
    println!("{}", solve_day1_p2(&test_data_path));

}

fn nums_from_string_p1(line : &str) -> Vec<u8> {
    let mut return_val = Vec::new();
    for char in line.as_bytes() {
        if char.is_ascii_digit() {
            return_val.push(char - '0' as u8);
        }
    }
    return_val
}

fn decode_line<F>(line : &str, num_funct : F) -> u64
    where F : Fn(&str)->Vec<u8> {
    let line_numbers = num_funct(line);
    if line_numbers.len() < 1 {
        panic!("Could not successfully parse line into numbers!");
    }
    (line_numbers[0] * 10 + line_numbers.last().unwrap()) as u64
}


fn split_into_lines(blob : &str) -> Vec<&str> {
    blob.split("\n").collect()
}

fn solve_day1_p1<P: AsRef<std::path::Path>>(test_file_path : P) -> u64 {
    // Open the test data file
    let test_file_data = std::fs::read_to_string(test_file_path).unwrap();
    let lines = split_into_lines(&test_file_data);
    lines.par_iter()
        .fold( || 0 as u64,|b, line| {
            decode_line(line, nums_from_string_p1) + b
        })
        .reduce( || 0_u64, |b, subsum| b + subsum)
}

fn nums_from_string_p2(line : &str) -> Vec<u8> {
    let mut return_val = Vec::new();
    let mut string_buff = String::new();
    for character in line.chars() {
        if character.is_ascii_digit() {
            return_val.push(character as u8 - '0' as u8);
            // Reset the buffer
            string_buff.clear();
        } else {
            // Add the character to the buffer
            string_buff.push(character);
            // Check to see if the character buffer contains a number
            let mut contain = false;
            if string_buff.contains("one") {return_val.push(1); contain = true;}
            else if string_buff.contains("two") {return_val.push(2); contain = true;}
            else if string_buff.contains("three") {return_val.push(3); contain = true;}
            else if string_buff.contains("four") {return_val.push(4); contain = true;}
            else if string_buff.contains("five") {return_val.push(5); contain = true;}
            else if string_buff.contains("six") {return_val.push(6); contain = true;}
            else if string_buff.contains("seven") {return_val.push(7); contain = true;}
            else if string_buff.contains("eight") {return_val.push(8); contain = true;}
            else if string_buff.contains("nine") {return_val.push(9); contain = true;}
            else {}
            if contain {
                string_buff.clear();
                string_buff.push(character);
            }
        }
        
    }
    return_val
}

fn solve_day1_p2<P: AsRef<std::path::Path>>(test_file_path : P) -> u64 {
    // Open the test data file
    let test_file_data = std::fs::read_to_string(test_file_path).unwrap();
    let lines = split_into_lines(&test_file_data);
    lines.par_iter()
        .fold( || 0 as u64,|b, line| {
            decode_line(line, nums_from_string_p2) + b
        })
        .reduce( || 0_u64, |b, subsum| b + subsum)
}

#[cfg(test)]
mod tests {
    use crate::nums_from_string_p2;

    #[test]
    fn one2onethree_test() {
        let test = "one2onethree";
        let answer = vec![1_u8, 2, 1, 3];
        let result = nums_from_string_p2(test);
        assert_eq!(answer, result);
    }

    #[test]
    fn three_test() {
        let test = "three";
        let answer = vec![3];
        let result = nums_from_string_p2(test);
        assert_eq!(answer, result);
    }

    #[test]
    fn crapone_test() {
        let test = "falkdflkjasdklfhbsonekdl;akevjfjke3";
        let answer = vec![1, 3];
        let result = nums_from_string_p2(test);
        assert_eq!(answer, result);
    }

    #[test]
    fn number_test() {
        let test = "3onetwothirteenfive";
        let answer = vec![3,1,2,5];
        let result = nums_from_string_p2(test);
        assert_eq!(answer, result);
    }

    #[test]
    fn test_three() {
        let test = "lentils4th4ree22fivet2wo";
        let answer = vec![4,4,2,2,5,2];
        let result = nums_from_string_p2(test);
        assert_eq!(answer, result);
    }

    #[test]
    fn test_twone() {
        let test = "twonefiveight";
        let answer = vec![2,1,5,8];
        let result = nums_from_string_p2(test);
        assert_eq!(answer, result);
    }
}