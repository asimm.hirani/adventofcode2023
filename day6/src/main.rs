use rayon::prelude::*;

fn main() {
    let input = std::fs::read_to_string("input/prod_input.txt").expect("Could not find file!");
    let input = Day6Input::from(input.as_str());
    let p2_input = std::fs::read_to_string("input/p2_input.txt").expect("Could not find file!");
    let p2_input = Day6Input::from(p2_input.as_str());
    let p1_answer = input.solve_p1();
    println!("Part 1 Answer: {p1_answer}");
    let p2_answer = p2_input.solve_p1();
    println!("Part 2 Answer: {p2_answer}");
}

struct Day6Input {
    race_times : Vec<u64>,
    distances : Vec<u64>,
}

impl From<&str> for Day6Input {
    fn from(value: &str) -> Self {
        // Parse the first line as a string of numbers
        let mut line_iter = value.lines();
        let line1 = line_iter.next().expect("Expected at least one line!");
        let line2 = line_iter.next().expect("Expected two lines!");

        let race_times : Vec<u64> = line1.split(':')
            .nth(1)
            .unwrap()
            .trim()
            .split_ascii_whitespace()
            .map(|substr| substr.parse::<u64>().unwrap())
            .collect();

        let distances : Vec<u64> = line2.split(':')
            .nth(1)
            .unwrap()
            .trim()
            .split_ascii_whitespace()
            .map(|substr| substr.parse::<u64>().unwrap())
            .collect();

        Day6Input {
            race_times,
            distances
        }
    }
}

impl Day6Input {
    fn solve_p1(&self) -> u64 {
        let mut number_of_wins = Vec::new();
        for (race_time, max_dist) in self.race_times.iter().zip(self.distances.iter()) {
            let race = Race(*race_time);
            let range = 0..race_time+1;
            let num_wins = range.into_par_iter().fold(|| 0_u64,
                |count, elem| if race.simulate(elem).unwrap_or(0) > *max_dist {count + 1} else {count})
                .reduce(|| 0_u64, |coll, elem| coll+elem);
            // for hold_time in 0..race_time+1 {
            //     if race.simulate(hold_time).unwrap_or(0) > *max_dist {
            //         num_wins += 1;
            //     }
            // }
            number_of_wins.push(num_wins);
        }
        number_of_wins.iter().product()
    }
}

struct Race (u64);

impl Race {
    fn simulate(&self, hold_time : u64) -> Option<u64> {
        let speed = hold_time;
        if hold_time > self.0 {
            return None;
        }
        Some(speed*(self.0-hold_time))
    }
}

