fn main() {
    println!("Hello, world!");

    let input_data = std::fs::read_to_string("data/prod_data.txt").unwrap();
    let p1 = solve_day3_p1(&input_data);
    let p2 = solve_day3_p2(&input_data);

    println!("p1: {}", p1);    
    println!("p2: {}", p2);
}

fn solve_day3_p1(data: &str) -> u64{
    let mut accumulator = 0;
    let lines: Vec<&[u8]> = data.split("\n").map(|line| line.as_bytes()).collect();

    let char_grid = CharacterGrid {
        lines: &lines
    };

    for (line_no, line) in lines.iter().enumerate() {
        let mut cursor = 0;
        while cursor < line.len() {
            if line[cursor].is_ascii_digit() {
                let (number, _, len) = get_number_and_len(line, cursor);
                if char_grid.is_symbol_around_substring(line_no, cursor, len) {
                    accumulator += number;
                }
                cursor += len;
            } else {
                cursor += 1;
            }
        }
    }
    accumulator
}

fn solve_day3_p2(data : &str) -> u64 {
    let mut accumulator = 0;
    let lines: Vec<&[u8]> = data.split("\n").map(|line| line.as_bytes()).collect();

    let char_grid = CharacterGrid {
        lines: &lines
    };

    for (line_no, line) in lines.iter().enumerate() {
        let mut cursor = 0;
        while cursor < line.len() {
            if line[cursor] == b'*' {
                let nums = char_grid.get_numbers_around_coordinate(line_no, cursor);
                if nums.len() == 2 {
                    accumulator += nums[0]*nums[1];
                }
            }
            cursor += 1;
        }
    }
    accumulator
}

/// Given a line and the start position which is a number, return the value
/// and number of characters of the number. If the character at start_pos is
/// not a number, panics.
fn get_number_and_len(line : &[u8], start_pos : usize) -> (u64, usize, usize) {
    if !line[start_pos].is_ascii_digit() {
        panic!("Character {} at position {} is not a number!", char::from(line[start_pos]), start_pos);
    }
    let mut string_stack = String::new();
    let mut cursor = start_pos;
    let mut length = 1;
    while cursor != 0 && line[cursor-1].is_ascii_digit() {
        cursor -= 1;
    }
    let start_pos = cursor;
    string_stack.push(line[cursor].into());
    cursor += 1;
    while cursor < line.len() && line[cursor].is_ascii_digit() {
        string_stack.push(line[cursor].into());
        cursor += 1;
        length += 1;
    }
    if let Ok(value) = string_stack.parse::<u64>() {
        (value, start_pos, length)
    } else {
        panic!("Could not parse '{}' into a number", string_stack);
    }
}

struct CharacterGrid<'a> {
    lines : &'a [&'a [u8]],
}

impl<'a> CharacterGrid<'a> {
    fn from_raw_data(data : &'a[&'a[u8]]) -> CharacterGrid {
        CharacterGrid {
            lines: data,
        }
    }
    fn is_symbol_around_substring(&self, line_no : usize, start_loc : usize, len : usize) -> bool {
        // Read the line before at the same location and see if it is a symbol
        let end_loc = start_loc + len - 1;
        // Grab the slice from above that we care about
        let above_slice = if line_no != 0 {
            let start = if start_loc == 0 {0} else {start_loc - 1};
            let end = if end_loc == self.lines[line_no].len()-1 {end_loc} else {end_loc + 1};
            &self.lines[line_no-1][start..end+1]
        } else {
            &[]
        };
        // Grab the left and right characters, else put in a dummy value
        let left_char = if start_loc != 0 {self.lines[line_no][start_loc-1]} else { b'0' };
        let right_char = if end_loc != self.lines[line_no].len()-1 {self.lines[line_no][end_loc+1]} else {b'0'};
        // Grab the slice below if it exists. 
        let below_slice = if line_no != self.lines.len()-1 {
            let start = if start_loc == 0 {0} else {start_loc - 1};
            let end = if end_loc == self.lines[line_no].len()-1 {end_loc} else {end_loc + 1};
            &self.lines[line_no+1][start..end+1]
        } else {
            &[]
        };

        // Concatenate all the slices
        let candidates = [above_slice, below_slice, &[left_char, right_char]].concat();
        // println!("above: {:?}, below: {:?}, left: {:?}, right: {:?}, line_len: {}, start_pos: {}, end_pos: {}", above_slice, below_slice, left_char, right_char, self.lines[line_no].len(), start_loc, end_loc);
        // If any of the candidates are symbols, return true, else false
        candidates.iter().map(|e| is_symbol(*e)).fold(false, |b, e| b | e)
    }

    fn get_numbers_around_coordinate(&self, line_no : usize, character : usize) -> Vec<u64> {
        let is_left_col = character == 0;
        let is_right_col = character == self.lines[line_no].len()-1;
        let is_top = line_no == 0;
        let is_bottom = line_no == self.lines.len()-1;

        let mut return_val = Vec::new();

        if !is_top {
            // Check line above
            if !is_right_col && self.lines[line_no-1][character+1].is_ascii_digit(){
                let (number, _, _) = get_number_and_len(self.lines[line_no-1], character+1);
                return_val.push(number);
            }
            if !is_left_col && self.lines[line_no-1][character-1].is_ascii_digit() {
                let (number, _, _) = get_number_and_len(self.lines[line_no-1], character-1);
                return_val.push(number);
            }
            if self.lines[line_no-1][character].is_ascii_digit() {
                let (number, _, _) = get_number_and_len(self.lines[line_no-1], character);
                return_val.push(number);
            }
        }
        if !is_bottom {
            // Check line above
            if !is_right_col && self.lines[line_no+1][character+1].is_ascii_digit(){
                let (number, _, _) = get_number_and_len(self.lines[line_no+1], character+1);
                return_val.push(number);
            }
            if !is_left_col && self.lines[line_no+1][character-1].is_ascii_digit() {
                let (number, _, _) = get_number_and_len(self.lines[line_no+1], character-1);
                return_val.push(number);
            }
            if self.lines[line_no+1][character].is_ascii_digit() {
                let (number, _, _) = get_number_and_len(self.lines[line_no+1], character);
                return_val.push(number);
            }
        }

        if !is_right_col && self.lines[line_no][character+1].is_ascii_digit(){
            let (number, _, _) = get_number_and_len(self.lines[line_no], character+1);
            return_val.push(number);
        }
        if !is_left_col && self.lines[line_no][character-1].is_ascii_digit() {
            let (number, _, _) = get_number_and_len(self.lines[line_no], character-1);
            return_val.push(number);
        }

        // Remove duplicate values, this is a hack because I noticed that numbers don't repeat around my input.
        return_val.dedup();

        return_val
    }
}

fn is_symbol(char_under_test : u8) -> bool {
    // is the character a .
    if char_under_test == b'.' { false }
    else if char_under_test.is_ascii_digit() { false }
    else { true }
}

#[cfg(test)]
mod tests {

    use super::*; 

    #[test]
    fn test_number_finding() {
        let input = "....432..33@..&..%....";
        let (number, _, len) = get_number_and_len(input.as_bytes(), 4);
        assert_eq!(number, 432);
        assert_eq!(len, 3);
    }

    #[test]
    fn test_number_finding2() {
        let input = "....432..33@..&..%.744";
        let (number, _, len) = get_number_and_len(input.as_bytes(), 19);
        assert_eq!(number, 744);
        assert_eq!(len, 3);
    }

    #[test]
    fn test_find_symbol_top_left() {
        let input =
            "...443@...33..&\n\
             ..22...77.3..4.\n\
             6&2.....5.....3";
        let lines : Vec<&[u8]> = input.lines().map(|e| e.as_bytes()).collect();
        let grid = CharacterGrid::from_raw_data(&lines);
        let result = grid.is_symbol_around_substring(1, 7, 2);
        assert_eq!(result,true);
    }

    #[test]
    fn test_find_symbol_top_right() {
        let input =
            "...443@...33..&\n\
             ..22...77.3..4.\n\
             6&2.....5.....3";
        let lines : Vec<&[u8]> = input.lines().map(|e| e.as_bytes()).collect();
        let grid = CharacterGrid::from_raw_data(&lines);
        let result = grid.is_symbol_around_substring(1, 13, 1);
        assert_eq!(result,true);
    }
}
